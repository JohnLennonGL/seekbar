package com.example.seekbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private SeekBar mSeekbar;
    private TextView mTextoExibicao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mSeekbar = findViewById(R.id.seekbarID);
        mTextoExibicao = findViewById(R.id.ExIbicaoID);


        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if(progress <= 10){
                    mTextoExibicao.setText("progresso: " + progress +"/" + seekBar.getMax());
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

               // Toast.makeText(MainActivity.this,"SeekBar foi acionado",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
               // Toast.makeText(MainActivity.this,"SeekBar foi interropido",Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void onClick(View view){

        mTextoExibicao.setText("Escolhido: " + mSeekbar.getProgress());

    }
}
